//
// Created by renyi on 2019/6/3.
//
#include <iostream>
#include <stack>
using namespace std;

class MinStack{
public:
    MinStack(){
    }

    void push(int x){
        _data.push(x);//数据栈正常把元素压栈
        if(_min.empty()){
            _min.push(x);//如果最小栈是空，没元素，直接压栈
        } else{
            if (x > _min.top()){
                x = _min.top();//如果x大于最小栈的栈顶，则取出最小栈的栈顶，赋值给x，以便后面的压栈
            }
            _min.push(x);//不管怎样，都会将当前最小的元素压栈，保证和数据栈元素个数相同
        }
    }

    void pop(){
        _data.pop();//出栈，大家都出栈，保持元素个数相同
        _min.pop();
    }

    int top(){
        return _data.top();//top就正常返回数据栈的栈顶
    }

    int getMin(){
        return _min.top();//getmin就是返回最小栈的栈顶
    }

private:
    stack<int> _data;//数据栈
    stack<int> _min;//最小栈
};

int main(){
    MinStack minStack;
    minStack.push(-3);//先压栈3，此时getmin应该是-3
    printf("top:[%d]\n", minStack.top());
    printf("min:[%d]\n\n", minStack.getMin());

    minStack.push(-5);//此时getmin应该是-5
    printf("top:[%d]\n", minStack.top());
    printf("min:[%d]\n\n", minStack.getMin());

    minStack.push(-7);//此时getmin应该是-7
    printf("top:[%d]\n", minStack.top());
    printf("min:[%d]\n\n", minStack.getMin());

    minStack.pop();//此时执行了pop（），-7出栈了，getmin应该是-5
    printf("top:[%d]\n", minStack.top());
    printf("min:[%d]\n\n", minStack.getMin());

    minStack.push(0);//0入栈，此时getmin还是-5
    printf("top:[%d]\n", minStack.top());
    printf("min:[%d]\n\n", minStack.getMin());
    return 0;
}