//
// Created by Renyi on 2019-09-10.
//
#include <iostream>
#include <vector>
using namespace std;

int maxSubArray(vector<int>& nums){
    vector<int> dp(nums.size(), 0);
    dp[0] = nums[0];
    int result = 0;
    for (int i = 1; i < nums.size(); ++i) {
        dp[i] = max(dp[i-1] + nums[i], nums[i]);
        if (dp[i] > result){
            result = dp[i];
        }
    }
    return result;
}

int main(){
    int arr[] = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    vector<int> nums(arr, arr+9);
    int result = maxSubArray(nums);
    cout << result << endl;
    return 0;
}

