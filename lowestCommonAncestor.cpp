//
// Created by renyi on 2019-07-04.
//
#include <iostream>
#include <vector>
using namespace std;

struct TreeNode{
    int value;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v) : value(v), left(NULL), right(NULL){}
};

//建立函数，找到从根节点root到待搜索节点search的路径
void preOrder(TreeNode* root, TreeNode* search, vector<TreeNode*> &path, vector<TreeNode*> &result, int &finish){
    if (!root || finish){//用finish来标识是否找到了待搜索节点，以免不必要的递归
        return;
    }

    path.push_back(root);
    if (root == search){
        finish = 1;//找到了待搜索节点
        result = path;
    }
    preOrder(root->left, search, path, result, finish);
    preOrder(root->right, search, path, result, finish);
    path.pop_back();
}

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q){
    vector<TreeNode*> path;//临时路径栈
    vector<TreeNode*> pathP;//存储p，q节点的路径
    vector<TreeNode*> pathQ;
    int finish = 0;
    preOrder(root, p, path, pathP, finish);
    path.clear();//求完p节点的路径，把临时路径栈path清空
    finish = 0;
    preOrder(root, q, path, pathQ, finish);//再求q节点路径

    int pathLen = pathP.size() <= pathQ.size() ? pathP.size() : pathQ.size();//存储较短路径的长度

    TreeNode* result = nullptr;
    for (int i = 0; i < pathLen; i++) {
        if (pathP[i] == pathQ[i]){
            result = pathP[i];
        }
    }
    return result;
}

int main(){
    TreeNode a(3);//建立配图的二叉树
    TreeNode b(5);
    TreeNode c(1);
    TreeNode d(6);
    TreeNode e(2);
    TreeNode f(0);
    TreeNode g(8);
    TreeNode h(7);
    TreeNode i(4);

    a.left = &b;
    a.right = &c;
    b.left = &d;
    b.right = &e;
    c.left = &f;
    c.right = &g;
    e.left = &h;
    e.right = &i;
    TreeNode* result = lowestCommonAncestor(&a, &b, &f);
    printf("%d ,%d 的最近公共祖先为: %d\n", b.value, f.value, result->value);

    result = lowestCommonAncestor(&a, &b, &i);
    printf("%d ,%d 的最近公共祖先为: %d\n", b.value, i.value, result->value);

    result = lowestCommonAncestor(&a, &h, &i);
    printf("%d ,%d 的最近公共祖先为: %d\n", h.value, i.value, result->value);

    return 0;
}
