//
// Created by renyi on 2019/6/20.
//
#include <iostream>
#include <string>
#include <vector>
using namespace std;

string manacherMaxPalindromeString(string s){
    string result = "";//带#号的最长回文串
    string result_str = "";//最终的最长回文串
    vector<char> tempStr;//字符数组，存储s加#号和$号后的字符串
    tempStr.push_back('$');
    for (int i = 0; i < s.length(); i++) {
        tempStr.push_back('#');
        tempStr.push_back(s[i]);
    }
    tempStr.push_back('#');

    int center = 0;//当前最长回文串的中心点
    int maxRight = 0;//当前最长回文串的右边界
    int n = tempStr.size();
    vector<int> p(n);//数组p，存储tempStr中以每个字符为中心点，最长回文串的长度
    for (int i = 1; i < tempStr.size(); i++) {
        string word = "";
        if (maxRight > i){
            p[i] = min(p[2 * center - i], maxRight - i);
        } else{
            p[i] = 1;
        }

        while (tempStr[i-p[i]] == tempStr[i+p[i]]){
            p[i]++;
        }

        for (int j = i-p[i]+1; j <= i+p[i]-1; ++j) {
            word += tempStr[j];
        }

        if (i + p[i] > maxRight){
            maxRight = i + p[i];
            center = i;
        }

        if (result.length() < word.length()){
            result = word;
        }
    }
    printf("result字符串为:%s, 需要去#号\n", result.c_str());
    for (int k = 0; k < result.size(); k++) {
        if (result[k] != '#'){
            result_str += result[k];
        }
    }
    return result_str;
}

int manacherMaxPalindromeLength(string s){
    int max_length = 0;//最终最长回文串的长度
    vector<char> tempStr;
    tempStr.push_back('$');
    for (int i = 0; i < s.length(); i++) {
        tempStr.push_back('#');
        tempStr.push_back(s[i]);
    }
    tempStr.push_back('#');

    int center = 0;//当前最长回文串的中心点
    int maxRight = 0;//当前最长回文串的右边界
    int n = tempStr.size();
    vector<int> p(n);//数组p，存储tempStr中，以每个字符为中心点，最长回文串的长度
    for (int i = 1; i < tempStr.size(); i++) {//先对p[i]的值做个界定，即预处理
        if (maxRight > i){
            p[i] = min(p[2 * center - i], maxRight - i);
        } else{
            p[i] = 1;
        }

        while (tempStr[i-p[i]] == tempStr[i+p[i]]){//界定完了之后，算出实际的p[i]的值，即若前后相等，即回文，那么p[i]累加
            p[i]++;
        }

        if (i + p[i] > maxRight){//更新右边界即中心点
            maxRight = i + p[i];
            center = i;
        }

        max_length = max(max_length, p[i] - 1);
    }
    return max_length;
}

int main(){
    string string1 = "abbacdedctgbbgtabba";
    string result_str = manacherMaxPalindromeString(string1);
    int result_len = manacherMaxPalindromeLength(string1);
    printf("最长回文字符串是: %s, 长度是: %d\n", result_str.c_str(), result_len);
    return 0;
}