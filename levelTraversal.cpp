//
// Created by renyi on 2019-07-09.
//
#include <iostream>
#include <queue>
using namespace std;

struct TreeNode{
    int value;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v) : value(v), left(NULL), right(NULL) {}
};

void levelTraversalPrint(TreeNode* root){
    queue<TreeNode*> q;
    q.push(root);
    while (!q.empty()){
        TreeNode* node = q.front();//取出队头元素
        printf("[%d]", node->value);
        q.pop();

        if (node->left){
            q.push(node->left);
        }

        if (node->right){
            q.push(node->right);
        }
    }
}

int main(){
    TreeNode a(1);//建立配图的二叉树
    TreeNode b(2);
    TreeNode c(5);
    TreeNode d(3);
    TreeNode e(4);
    TreeNode f(6);

    a.left = &b;
    a.right = &c;
    b.left = &d;
    b.right = &e;
    c.right = &f;

    levelTraversalPrint(&a);
    return 0;
}
