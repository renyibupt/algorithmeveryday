//
// Created by renyi on 2019/6/19.
// 单纯判断一个字符串是否为回文串
//
#include <iostream>
#include <string>
using namespace std;

bool palindromeString(string s){
    int i = 0;
    int j = s.length() - 1;
    while (i <= j){
        if (s[i] == s[j]){
            i += 1;
            j -= 1;
        } else{
            return false;
        }
    }
    return true;
}
int main(){
    string a = "aba";
    cout << palindromeString(a) << endl;
    return 0;
}