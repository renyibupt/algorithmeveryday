//
// Created by renyi on 2019-08-02.
//
#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct TreeNode{
    int value;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v) : value(v), left(nullptr), right(nullptr){}
};

int findKthLevelNodes(TreeNode* root, int k){
    int count = 0;
    queue<pair<TreeNode*, int>> q;//建立队列q，保存<节点，层数>绑定在一起的数据

    if (root){//当根节点非空时，将根节点和层数0<root, 1>push进队列
        q.push(make_pair(root, 1));
    }

    while (!q.empty()){
        TreeNode* node = q.front().first;
        int depth = q.front().second;
        q.pop();

        if (depth == k){
            count += 1;
        }

        if (node->left){
            q.push(make_pair(node->left, depth+1));
        }

        if (node->right){
            q.push(make_pair(node->right, depth+1));
        }

    }
    return count;
}

int main(){
    TreeNode a(1);
    TreeNode b(2);
    TreeNode c(3);
    TreeNode d(4);
    TreeNode e(5);
    TreeNode f(6);
    TreeNode g(7);
    TreeNode h(8);
    TreeNode i(9);
    TreeNode j(10);
    TreeNode k(11);
    TreeNode l(12);

    a.left = &b;
    a.right = &c;
    b.left = &d;
    b.right = &e;
    c.left = &f;
    c.right = &g;
    d.left = &h;
    d.right = &i;
    e.left = &j;
    e.right = &k;
    f.left = &l;

    int result = findKthLevelNodes(&a, 4);
    printf("该层节点个数为: %d\n", result);

    return 0;
}
