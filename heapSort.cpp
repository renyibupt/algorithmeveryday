//
// Created by renyi on 2019-07-14.
//
#include <iostream>
#include <vector>
using namespace std;

void printArray(vector<int> nums){
    for (int i = 0; i < nums.size(); i++) {
        printf("[%d]", nums[i]);
    }
    printf("\n");
}

void swap(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}

void adjustHeap(vector<int>& nums, int parNode, int high){
    int newParNode = parNode;
    int j = 2 * parNode + 1;//取根节点的左孩子

    while (j <= high){//如果j=high，说明没有右孩子，high就是左孩子
        if (j < high && nums[j] <= nums[j+1]){
            j += 1;//一个根节点下如果有两个孩子，将j指向更大的那个孩子
        }

        if (nums[j] >= nums[newParNode]){//如果j指向的孩子大于父节点，就交换
            swap(nums[j], nums[newParNode]);
            newParNode = j;
            j = j * 2 + 1;
        } else{
            break;
        }
    }
}

void heapSort(vector<int>& nums){
    int last = nums.size() - 1;
    int lastParNode = nums.size() / 2 - 1;//最后一个父节点

    while (lastParNode >= 0){
        adjustHeap(nums, lastParNode, last);
        lastParNode -= 1;//建立最大堆，是从后往前调整，每调整好一个节点，就从后往前移动一个节点
    }

    //上面的while循环结束之后，即建立起了一个最大堆
    //接下来开始堆排序，精髓就是，交换头和尾，把最大的放最后面，再调整为最大堆，继续交换
    while (last > 0){
        swap(nums[0], nums[last]);
        adjustHeap(nums, 0, last-1);//从前往后调整最大堆
        last -= 1;
    }
}

int main(){
    vector<int> nums;
    nums.push_back(4);
    nums.push_back(7);
    nums.push_back(0);
    nums.push_back(9);
    nums.push_back(1);
    nums.push_back(5);
    nums.push_back(3);
    nums.push_back(3);
    nums.push_back(2);
    nums.push_back(6);

    printArray(nums);
    heapSort(nums);
    printArray(nums);
    return 0;
}

