//
// Created by renyi on 2019/6/17.
//
#include <iostream>
#include <string>
using namespace std;

string longestSubString(string s){
    int begin = 0;//窗口的头指针，即窗口左边界
    string word = "";//记录当前满足条件的最长子串
    string result = "";//记录结果
    int char_map[128] = {0};//记录字符数量的字符哈希

    for (int i = 0; i < s.length(); i++) {
        char_map[s[i]]++;
        if (char_map[s[i]] == 1){//word中没出现过该字符
            word += s[i];//把当前字符添加进word
            if (result.length() < word.length()){
                result = word;//如果word长度超过了当前result长度，则重新赋值result
            }
        } else{//当word中重复出现了该字符
            while (begin < i && char_map[s[i]] > 1){
                char_map[s[begin]]--;//当该字符的字符哈希值，即数量大于1的时候，将begin持续向右移动，知道begin与i维护的滑动窗口中，该字符数量为1
                begin++;
            }
            word = "";
            for (int j = begin; j <= i; j++) {
                word += s[j];//重置word为begin与i维护的滑动窗口之间的字符串，此时word内不包含重复字符，继续向后遍历
            }
        }
    }
    return result;
}

int main(){
    string string1 = "abcdabcabbcdefgaaaaaaa";
    string result = longestSubString(string1);
    cout << result << endl;
    return 0;
}