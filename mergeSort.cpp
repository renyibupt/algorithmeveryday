//
// Created by renyi on 2019-07-22.
//
#include <iostream>
#include <vector>
using namespace std;

void mergeTwoSortedVector(vector<int>& vec1, vector<int>& vec2, vector<int>& vec){
    int i = 0;//遍历vec1的下标
    int j = 0;//遍历vec2的下标

    while (i < vec1.size() && j < vec2.size()){
        if (vec1[i] <= vec2[j]){
            vec.push_back(vec1[i]);
            i++;
        } else{
            vec.push_back(vec2[j]);
            j++;
        }
    }

    for (; i < vec1.size(); i++) {//谁更长，直接把剩余部分追加进去vec
        vec.push_back(vec1[i]);
    }
    for (; j < vec2.size(); j++) {
        vec.push_back(vec2[j]);
    }
}

void mergeSort(vector<int>& vec){
    if (vec.size() < 2){
        return;
    }

    int mid = vec.size() / 2;
    vector<int> vec1;
    vector<int> vec2;

    for (int i = 0; i < mid; i++) {
        vec1.push_back(vec[i]);
    }
    for (int j = mid; j < vec.size(); j++) {
        vec2.push_back(vec[j]);
    }

    mergeSort(vec1);
    mergeSort(vec2);
    vec.clear();
    mergeTwoSortedVector(vec1, vec2, vec);
}

int main(){
    int temp[] = {7, 9, 5, 1, 2, 8, 6, 4, 0, 3, 10};
    vector<int> result;
    for (int i = 0; i < 11; i++) {
        result.push_back(temp[i]);
    }

    mergeSort(result);

    for (int i = 0; i < result.size(); i++) {
        printf("[%d]", result[i]);
    }
    printf("\n");
    return 0;
}
