//
// Created by renyi on 2019/6/24.
//
#include <iostream>
#include <vector>
using namespace std;

void findSubsets(int i, vector<int> &nums, vector<int> &item, vector<vector<int>> &result){
    if (i >= nums.size()){
        return;
    }
    item.push_back(nums[i]);//将nums的每个元素放入数组item
    result.push_back(item);//将item数组，放入最后的二维数组result
    findSubsets(i + 1, nums, item, result);//放入一个元素后，递归进行后续元素的选择
    item.pop_back();//将该元素拿出来
    findSubsets(i + 1, nums, item, result);//再递归跑一次，不放入该元素的情况下，对于后续元素的处理
}

vector<vector<int>> subsets(vector<int> &nums){
    vector<vector<int>> result;//最终结果，二维数组
    vector<int> item;//一维数组item
    result.push_back(item);
    findSubsets(0, nums, item, result);
    return result;
}

int main(){
    vector<int> nums;
    nums.push_back(1);
    nums.push_back(2);
    nums.push_back(3);
    vector<vector<int>> result;
    result = subsets(nums);
    for (int i = 0; i < result.size(); i++) {
        if (result[i].size() == 0){
            printf("[]");
        }
        for (int j = 0; j < result[i].size(); j++) {
            printf("[%d]", result[i][j]);
        }
        printf("\n");
    }
    return 0;
}