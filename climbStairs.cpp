//
// Created by Renyi on 2019-10-17.
//
#include <iostream>
#include <vector>
using namespace std;

int climbStairs(){
    vector<int> dp(14, 0);
    dp[0] = 1;
    dp[1] = 2;
    dp[2] = 4;
    for (int i = 3; i < 11; i++){
        dp[i] = dp[i-1] + dp[i-2] + dp[i-3];
    }
    return dp[10];
}

int main(){
    int res = climbStairs();
    cout << res << endl;
    return 0;
}
