//
// Created by renyi on 2019/5/29.
//
#include <iostream>
using namespace std;

struct Node{
    int value;
    Node* next;
    Node(int v):value(v),next(NULL){}
};

Node* reverse(Node* head, int m, int n){
    int changeLen = n - m + 1;//需要逆置的节点的个数
    Node* preHead = NULL;//初始化，开始逆置的节点的前驱
    Node* result = head;//该函数最终返回的头节点，非特殊情况就是head

    while (head && --m){
        preHead = head;//将head移动m-1个位置，此时head指向开始逆置的节点
        head = head->next;
    }
    Node* modifyListTail = head;//将modifyListTail，即逆置后尾节点指向head
    Node* newHead = NULL;

    while (head && changeLen){//该函数完成逆置n-m+1个节点
        Node* next = head->next;
        head->next = newHead;
        newHead = head;
        head = next;
        changeLen--;
    }
    modifyListTail->next = head;
    if (preHead){//如果preHead不为空，那么就不是从第一个节点开始逆置的
        preHead->next = newHead;
    }
    else{//如果preHead是空，那就是从第一个节点开始逆置的，那么逆置段中，逆置后的头节点，就是要返回的头节点
        result = newHead;
    }
    return result;
}

void destroy(Node* head){
    Node* next;
    while(head){
        next = head->next;
        delete(head);
        head = next;
    }
}
void print(Node* head){
    while(head){
        cout<<head->value<<"->";
        head = head->next;
    }
    cout<<endl;
}

int main(){
    Node* head = new Node(1);
    for (int i = 0; i < 10; ++i) {
        Node* p = new Node(rand() % 100);
        p->next = head->next;
        head->next = p;
    }
    print(head);
    reverse(head, 10, 11);
    print(head);
    destroy(head);
}