//
// Created by renyi on 2019/6/4.
//
#include <iostream>
#include <queue>
#include <vector>
using namespace std;

int findKthLargest(vector<int>& nums, int k){
    priority_queue<int, vector<int>, greater<int> > less_heap;//初始化一个最小堆
    for (int i = 0; i < nums.size(); i++) {
        if (less_heap.size() < k){//如果最小堆中元素个数，小于k个，直接push入堆
            less_heap.push(nums[i]);//保证是一个k大小的最小堆
        } else if(less_heap.top() < nums[i]){
            less_heap.pop();//当堆顶元素小于当前元素，那么当前元素更大一些，所以push入堆。当然首先将堆顶元素弹出。
            less_heap.push(nums[i]);
        }
    }
    return less_heap.top();//堆顶即为第K大的数
}

int main(){
    vector<int> nums;
    for (int i = 0; i < 10; i++){
        nums.push_back(rand() % 100);
    }
    printf("the start nums is:");
    for (int i = 0; i < 10; i++) {
        printf("%d ", nums[i]);
    }
    printf("\n");
    int result = findKthLargest(nums, 4);
    printf("the 4th largest num is:%d\n", result);
    return 0;
}
