//
// Created by renyi on 2019/6/19.
//
#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool is_window_ok(int map_s[], int map_t[], vector<int>& vec_t){//检查此时窗口是否仍然满足条件的函数
    for (int i = 0; i < vec_t.size(); i++) {
        if (map_s[vec_t[i]] < map_t[vec_t[i]]) {
            return false;//遍历t中的字符，如果此时s中该字符的数量，比t中该字符的数量还要少，就不满足窗口条件了
        }
    }
    return true;
}

string minWindowSubString(string s, string t){
    int map_s[128] = {0};//利用0~127的数组下标来记录字符个数，记录字符串s中各字符的数量
    int map_t[128] = {0};//记录t的各字符数量
    vector<int> vec_t;

    for (int i = 0; i < t.length(); i++) {
        map_t[t[i]]++;//记录字符串t中各字符数量
    }

    for (int i = 0; i < 128; i++) {
        if (map_t[i] > 0){
            vec_t.push_back(i);//记录字符串t中，都有哪些字符
        }
    }

    int begin = 0;
    string result;
    for (int i = 0; i < s.length(); i++) {
        map_s[s[i]]++;
        while (begin < i){
            char begin_char = s[begin];
            if (map_t[begin_char] == 0){//即，s的begin下标对应的字符，根本不在t中，直接begin后移
                begin++;
            }
            else if (map_s[begin_char] > map_t[begin_char]){//s的当前字符比t的多，即不需要那么多，将begin后移，当前字符数量减1
                map_s[begin_char]--;
                begin++;
            }
            else {
                break;//其他情况均不用移动begin的位置
            }
        }
        if (is_window_ok(map_s, map_t, vec_t)){
            int new_window_len = i - begin + 1;//新窗口字符串的长度
            if (result == "" || result.length() > new_window_len){
                result = s.substr(begin, new_window_len);
            }
        }
    }

    return result;
}
int main(){
    string string1 = "ADOBECODEBANC";
    string result = minWindowSubString(string1, "AC");
    printf("%s\n", result.c_str());
    return 0;
}