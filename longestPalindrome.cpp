//
// Created by renyi on 2019-06-12.
//
#include <iostream>
#include <string>
using namespace std;

int longestPalindrome(string s){
    int char_map[128] = {0};//初始化字符哈希表
    int max_length = 0;//字符串s的偶数长度字符部分
    int flag = 0;//flag为是否有中心字符标志位

    for (int i = 0; i < s.length(); i++) {
        char_map[s[i]]++;//利用char_map数组的下标实现字符哈希，来统计字符串s中每个字符的个数
    }

    for (int i = 0; i < 128; i++) {
        if (char_map[i] % 2 == 0){
            max_length += char_map[i];//偶数个数的字符直接算进总数
        } else{
            max_length += char_map[i] - 1;//奇数个数的字符，减1个，使成为偶数个数
            flag = 1;//同时置标志位为1
        }
    }
    return max_length + flag;//最终返回偶数个数与标志位的和
}

int main(){
    string s = "abccccdddaaaffggg";
    int result = longestPalindrome(s);
    printf("该字符串的最长回文串长度为:%d\n", result);
    return 0;
}

