//
// Created by renyi on 2019-07-08.
//
#include <iostream>
#include <vector>
using namespace std;

struct TreeNode{
    int value;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v) : value(v), left(NULL), right(NULL) {}
};

//先写通过申请额外空间的算法，比较简单，不写注释了
void preOrderExtraSpace(TreeNode* root, vector<TreeNode*> &nodeVec){
    if (!root){
        return;
    }
    nodeVec.push_back(root);
    preOrderExtraSpace(root->left, nodeVec);
    preOrderExtraSpace(root->right, nodeVec);
}

void flattenExtraSpace(TreeNode* root){
    vector<TreeNode*> nodeVec;//初始化一个存放节点的数组
    preOrderExtraSpace(root, nodeVec);
    for (int i = 1; i < nodeVec.size(); i++) {
        nodeVec[i-1]->left = nullptr;//左指针赋值空，右指针指向后面的节点，即将节点链起来
        nodeVec[i-1]->right = nodeVec[i];
    }
}

//接下来是不申请额外空间，原地将二叉树转单链表
void preOrder(TreeNode* root, TreeNode*& last){//last指针传引用是为了，传给下一次递归用
    if (!root){
        return;
    }
    if (!root->left && !root->right){//如果是叶子节点，那它就是last指针
        last = root;//如果左右都是叶子，那last最终会递归到，最后边的叶子
        return;//如果左右只有一个叶子，那last就是叶子
    }

    TreeNode* left = root->left;//备份左右指针
    TreeNode* right = root->right;
    TreeNode* leftLast = nullptr;
    TreeNode* rightLast = nullptr;

    if (left){//如果有左子树，就先一路递归左子树，直到叶子节点
        preOrder(left, leftLast);
        root->left = nullptr;//当前节点的左指针赋空，统一用右指针root->right连接成单链表
        root->right = left;//这个left就是出递归的left，就是左子树（如果有左子树）的叶子节点
        last = leftLast;//将出递归的leftLast,赋值给last指针，以进行后面的递归
    }

    if (right){//如果有右子树，就继续递归右子树
        preOrder(right, rightLast);
        if (leftLast){//如果有leftLast，就说明进行了上面的if里面的递归，即说明，有左子树，并且leftLast是左子树的叶子节点
            leftLast->right = right;//将左子树的叶子节点的后继，指向右子树，即完成链表的连接步骤
        }
        last = rightLast;//将右子树（如果有右子树）出递归时的rightLast赋给last，来进行后面的递归
    }
}

void flatten(TreeNode* root){
    TreeNode* last = nullptr;
    preOrder(root, last);
}

int main(){
    TreeNode a(1);//建立配图的二叉树
    TreeNode b(2);
    TreeNode c(5);
    TreeNode d(3);
    TreeNode e(4);
    TreeNode f(6);

    a.left = &b;
    a.right = &c;
    b.left = &d;
    b.right = &e;
    c.right = &f;

    flatten(&a);
    TreeNode* head = &a;
    while (head){
        if (head->left){
            printf("ERROR\n");
        } else{
            printf("[%d]", head->value);
            head = head->right;
        }
    }
    printf("\n");
    return 0;
}
