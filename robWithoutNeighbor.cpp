//
// Created by Renyi on 2019-08-21.
//
#include <iostream>
#include <vector>
using namespace std;

int robWithoutNeighbor(vector<int>& nums){
    if (nums.size() == 0){
        return 0;
    }
    if (nums.size() == 1){
        return nums[0];
    }

    vector<int> dp(nums.size(), 0);
    dp[0] = nums[0];
    dp[1] = max(nums[0], nums[1]);

    for (int i = 2; i < nums.size(); ++i) {
        dp[i] = max(dp[i-1], dp[i-2] + nums[i]);
    }

    return dp[nums.size()-1];
}

int main(){
    vector<int> nums;
    int a[] = {5, 3, 6, 7, 9, 2};
    for (int i = 0; i < 6; ++i) {
        nums.push_back(a[i]);
    }

    int result = robWithoutNeighbor(nums);
    cout<<result<<endl;
}

