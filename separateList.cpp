//
// Created by 任毅 on 2019-06-01.
//
#include <iostream>
using namespace std;

struct Node{
    int value;
    Node* next;
    Node(int v):value(v),next(NULL){}
};

Node* separateList(Node* head, int x){
    Node less_head(0);//创建两个临时头节点
    Node more_head(0);

    Node* less = &less_head;//对应指针指向这两个临时头节点
    Node* more = &more_head;

    while(head){
        if (head->value < x){//若节点值小于x，应插到less_head后面
            less->next = head;
            less = head;//链接完节点以后，应该将less后移，指向head
        } else{
            more->next = head;//若节点值大于x，应插到more_head后面
            more = head;//链接完节点以后，应该将less后移，指向head
        }
        head = head->next;
    }
    less->next = more_head.next;//此时less指针的后继应指向，more_head头节点的后继，即more_head部分第一个节点
    more->next = NULL;//此时more指针遍历到链表尾，后继置空
    return less_head.next;
}

void print(Node* head){//对比第一天的print函数，改进了一下
    while(head){
        if (head->next){
            cout<<head->value<<"->";
        } else{
            cout<<head->value;
        }
        head = head->next;
    }
    cout<<endl;
}

int main(){
    Node a(1);
    Node b(6);
    Node c(2);
    Node d(7);
    Node e(10);
    Node f(3);
    Node g(4);
    Node h(9);
    a.next = &b;
    b.next = &c;
    c.next = &d;
    d.next = &e;
    e.next = &f;
    f.next = &g;
    g.next = &h;

    Node* head = separateList(&a, 5);
    print(head);
    return 0;
}
