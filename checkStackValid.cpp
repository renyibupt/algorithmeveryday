//
// Created by renyi on 2019-07-14.
//
#include <iostream>
#include <vector>
#include <stack>
using namespace std;

int checkStackValid(vector<int>& enter, vector<int>& outer){
    vector<int> num;
    int j = 0;

    for (int i = 0; i < enter.size(); i++) {
        num.push_back(enter[i]);
        while (num[num.size()-1] == outer[j] && j < enter.size()){
            num.pop_back();
            j += 1;
        }
    }

    if (num.empty()){
        return 1;
    } else{
        return 0;
    }
}

int main(){
    vector<int> enter;
    vector<int> outer;

    enter.push_back(1);
    enter.push_back(2);
    enter.push_back(3);
    enter.push_back(4);
    enter.push_back(5);

    outer.push_back(4);
    outer.push_back(5);
    outer.push_back(2);
    outer.push_back(3);
    outer.push_back(1);

    int result = checkStackValid(enter, outer);
    cout << result << endl;
    return 0;
}
