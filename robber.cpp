//
// Created by Renyi on 2019-11-14.
//
#include <iostream>
#include <vector>
using namespace std;

int robber(vector<int>& nums){
    int n = nums.size();
    if (n == 0){
        return -1;
    }
    vector<int> dp(n, 0);
    dp[0] = nums[0];
    dp[1] = max(nums[1], nums[0]);
    for (int i = 2; i < n; ++i) {
        dp[i] = max(dp[i-1], dp[i-2]+nums[i]);
    }
    return dp[n-1];
}

int main(){
    int a[] = {5, 3, 6, 7, 9, 2};
    vector<int> nums(a, a+6);
    int res = robber(nums);
    cout << res << endl;
    return 0;
}
