//
// Created by renyi on 2019-06-14.
//
#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;

vector<vector<string>> groupAnagram(vector<string>& strings){//这个函数，参数是字符串数组，返回类型是二位字符串数组
    map<string, vector<string>> anagram;//初始化一个哈希map，从字符串到字符串数组的映射
    vector<vector<string>> result;//存储最终的结果，即二位字符串数组

    for (int i = 0; i < strings.size(); i++) {//遍历strings里的每个单词
        string str = strings[i];//创建一个临时字符串变量str接收每个单词
        sort(str.begin(), str.end());//对单词进行排序

        if (anagram.find(str) == anagram.end()) {//如果排序后的单词str，不在哈希map里
            vector<string> temp;//创建一个空的字符串数组
            anagram[str] = temp;//以排序后的strings[i]作key
        }
        anagram[str].push_back(strings[i]);//在key对应的value中push当前单词
    }

    map<string, vector<string>> ::iterator it;//初始化一个指向，从字符串string，到字符串数组vector<string>的哈希map的指针it
    for (it = anagram.begin(); it != anagram.end(); it++) {
        result.push_back(it->second);//遍历哈希map：anagram，将其value，push进result
    }
    return result;
}

int main(){
    vector<string> strings;
    strings.emplace_back("nba");
    strings.emplace_back("dog");
    strings.emplace_back("god");
    strings.emplace_back("abn");
    strings.emplace_back("bna");
    strings.emplace_back("bat");
    
    vector<vector<string>> result = groupAnagram(strings);

    for (int i = 0; i < result.size(); i++) {
        for (int j = 0; j < result[i].size(); j++) {
            printf("[%s]", result[i][j].c_str());
        }
        printf("\n");
    }
    return 0;
}

