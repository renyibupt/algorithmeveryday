//
// Created by renyi on 2019/6/18.
//
#include <iostream>
#include <string>
#include <vector>
#include <map>
using namespace std;

vector<string> findRepeatedDnaSequences(string s){
    map<string, int> word_map;//建立<单词，单词数量>的哈希map
    vector<string> result;//最终返回字符串数组

    for (int i = 0; i < s.length(); i++) {
        string word = s.substr(i, 10);//从下标i开始，取长度为10的子串
        if (word_map.find(word) != word_map.end()){//如果单词word在哈希map中出现了
            word_map[word] += 1;//累加出现次数
        } else{
            word_map[word] = 1;
        }
    }
    //for循环结束后，已遍历完字符串，接下来统计哈希map中出现次数大于1的子串
    map<string, int>::iterator it;
    for (it = word_map.begin(); it != word_map.end() ; it++) {
        if (it->second > 1){
            result.push_back(it->first);
        }
    }
    return result;
}

int main(){
    string string1 = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";
    vector<string> result = findRepeatedDnaSequences(string1);
    for (int i = 0; i < result.size(); i++) {
        printf("%s\n", result[i].c_str());
    }
    return 0;
}