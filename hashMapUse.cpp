//
// Created by renyi on 2019-06-13.
//
#include <iostream>
#include <map>
#include <string>
using namespace std;

int main(){
    map<string, int> hash_map;//初始化一个字符串string到整型int的映射，即哈希map
    string string1 = "abc";
    string string2 = "aaa";
    string string3 = "wwwwwww";
    //建立了三个字符串到整型的映射
    hash_map[string1] = 1;
    hash_map[string2] = 2;
    hash_map[string3] = 3;

    if (hash_map.find(string1) != hash_map.end()){//如果在哈希map中找到了string1
        printf("%s is in hash map, and it's value is:%d\n", string1.c_str(), hash_map[string1]);
    }

    map<string, int> ::iterator it;//初始化一个迭代器指针it，指向一个从string到int的映射
    for (it = hash_map.begin(); it != hash_map.end(); it++) {//it指向了hash_map
        printf("hash_map[%s] = %d\n", it->first.c_str(), it->second);//it->first就是当前的映射的第一个元素，就是字符串string，it->second同理
    }
    return 0;
}

