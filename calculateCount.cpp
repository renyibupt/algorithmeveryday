//
// Created by renyi on 2019-06-12.
//
#include <iostream>
#include <string>
using namespace std;

void calculateCount(string s){
    int char_map[128] = {0};
    for (int i = 0; i < s.length(); i++) {
        char_map[s[i]]++;
    }

    for (int i = 0; i < 128; i++) {
        if (char_map[i] > 0){
            printf("[%c][%d] : %d\n", i, i, char_map[i]);
        }
    }
}
int main(){
    string s = "abcdefgabcdaaagg";
    calculateCount(s);
    return 0;
}
