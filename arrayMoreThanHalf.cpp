//
// Created by Renyi on 2019-11-19.
//
#include <iostream>
#include <vector>
using namespace std;

int arrayMoreThanHalf(vector<int>& nums){
    if (nums.size() == 0){
        return -1;
    }

    int curNum = nums[0];
    int count = 1;
    for (int i = 1; i < nums.size(); ++i) {
        if (nums[i] == curNum){
            count += 1;
        } else{
            if (count > 0){
                count -= 1;
            } else{
                curNum = nums[i];
                count += 1;
            }
        }
    }
    return curNum;
}

int main(){
    int arr[] = {5,6,5,5,5,6,6,6,6};
    vector<int> nums(arr, arr+9);
    int res = arrayMoreThanHalf(nums);
    cout << res << endl;
    return 0;
}
