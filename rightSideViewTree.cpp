//
// Created by renyi on 2019-07-09.
//
#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct TreeNode{
    int value;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v) : value(v), left(NULL), right(NULL) {}
};

vector<int> rightSideViewTree(TreeNode* root){
    vector<int> view;//建立数组view，保存二叉树的各层最右边的节点
    queue<pair<TreeNode*, int>> q;//建立队列q，保存<节点，层数>绑定在一起的数据

    if (root){//当根节点非空时，将<root, 0>push进入队列
        q.push(make_pair(root, 0));
    }

    while (!q.empty()){//当队列非空时，一直循环
        TreeNode* node = q.front().first;//取队头的第一个元素，第一次取，就是根节点指针root
        int depth = q.front().second;//取队头第二个元素，第一次取，就是根节点高度，就是0
        q.pop();

        if (view.size() == depth){//当view.size()=高度depth时，说明又往深遍历了一层，此时要往view中添加节点
            view.push_back(node->value);
        } else{//只要不相等，说明遍历的节点还在同一层，更新下标为depth的值，即更新更靠右的节点值
            view[depth] = node->value;
        }

        if (node->left){//层次遍历，将左右子树添加进队列，同时高度+1
            q.push(make_pair(node->left, depth+1));
        }
        if (node->right){
            q.push(make_pair(node->right, depth+1));
        }
    }
    return view;
}

int main(){
    TreeNode a(1);//建立配图的二叉树
    TreeNode b(2);
    TreeNode c(5);
    TreeNode d(3);
    TreeNode e(4);
    //TreeNode f(6);

    a.left = &b;
    a.right = &c;
    b.left = &d;
    b.right = &e;
    //c.right = &f;

    vector<int> result = rightSideViewTree(&a);
    for (int i = 0; i < result.size(); i++) {
        printf("[%d]", result[i]);
    }
    printf("\n");
    return 0;
}
