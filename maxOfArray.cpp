//
// Created by renyi on 2019-07-13.
//
#include <iostream>
#include <vector>
using namespace std;

int maxOfArray(vector<int> nums){
    int maxSum = nums[0];
    int curSum = nums[0];

    for (int i = 1; i < nums.size(); i++) {
        curSum = nums[i] >= (curSum + nums[i]) ? nums[i] : (curSum + nums[i]);
        maxSum = maxSum >= curSum ? maxSum : curSum;
    }
    return maxSum;
}

int main(){
    vector<int> nums;
    nums.push_back(-2);
    nums.push_back(1);
    nums.push_back(-3);
    nums.push_back(4);
    nums.push_back(1);
    nums.push_back(2);
    nums.push_back(1);
    nums.push_back(-5);
    nums.push_back(-4);

    int result = maxOfArray(nums);
    printf("%d\n", result);
    return 0;
}
