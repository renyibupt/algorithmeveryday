//
// Created by renyi on 2019-07-03.
//
#include <iostream>
#include <vector>
using namespace std;

struct TreeNode{//二叉树的每个节点的数据结构
    int value;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v) : value(v), left(NULL), right(NULL) {}
};

void preOrder(TreeNode* root, int &pathValue, int sum, vector<int> &path, vector<vector<int>> &result){
    if (!root){
        return;
    }

    pathValue += root->value;//遍历一个节点就更新一下路径值
    path.push_back(root->value);//将节点值push进路径栈path
    if (!root->left && !root->right && pathValue == sum){
        result.push_back(path);
    }

    preOrder(root->left, pathValue, sum, path, result);
    preOrder(root->right, pathValue, sum, path, result);

    pathValue -= root->value;//这里出递归，向上回溯，所以减去节点值
    path.pop_back();//并将当前节点弹出路径栈path
}

vector<vector<int>> pathSum(TreeNode* root, int sum){
    vector<vector<int>> result;//初始化二维数组result，存储最后结果
    vector<int> path;//路径栈path
    int pathValue = 0;
    preOrder(root, pathValue, sum, path, result);
    return result;
}

int main(){
    TreeNode a(5);//建立配图的二叉树
    TreeNode b(4);
    TreeNode c(8);
    TreeNode d(11);
    TreeNode e(13);
    TreeNode f(4);
    TreeNode g(7);
    TreeNode h(2);
    TreeNode i(5);
    TreeNode j(1);

    a.left = &b;
    a.right = &c;
    b.left = &d;
    c.left = &e;
    c.right = &f;
    d.left = &g;
    d.right = &h;
    f.left = &i;
    f.right = &j;
    vector<vector<int>> result = pathSum(&a, 22);
    for (int m = 0; m < result.size(); m++) {
        for (int n = 0; n < result[m].size(); n++) {
            printf("[%d]", result[m][n]);
        }
        printf("\n");
    }
    return 0;
}