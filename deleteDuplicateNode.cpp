//
// Created by renyi on 2019-06-01.
//
#include <iostream>
using namespace std;

struct Node{
    int value;
    Node* next;
    Node(int v):value(v),next(NULL){}
};

Node* deleteDuplicateNode(Node* head){
    Node* pre = head;//初始化前驱指针
    Node* cur;//初始化当前指针

    while(pre){
        cur = pre->next;
        if (cur && (cur->value == pre->value)){
            pre->next = cur->next;//若发现重复，则将前驱指针pre的后继，赋值为当前指针cur的后继
        } else{
            pre = cur;
        }
    }
    return head;
}

Node* deleteAllDuplicateNode(Node* head){
    Node* pre = head;
    Node* cur = pre->next;
    Node* next;
    bool dup;
    while(cur){
        next = cur->next;
        dup = false;
        while(next && cur->value == next->value){
            pre->next = next;
            cur = next;
            next = cur->next;
            dup = true;
        }
        if (dup){
            pre->next = next;
        } else{
            pre = cur;
        }
        cur = next;
    }
    return head;
}

void print(Node* head){
    while(head){
        if (head->next){
            cout<<head->value<<"->";
        } else{
            cout<<head->value;
        }
        head = head->next;
    }
    cout<<endl;
}

int main(){
    Node a(2);
    Node b(3);
    Node c(3);
    Node d(5);
    Node e(7);
    Node f(8);
    Node g(8);
    Node h(8);
    Node i(9);
    Node j(9);
    Node k(10);

    a.next = &b;
    b.next = &c;
    c.next = &d;
    d.next = &e;
    e.next = &f;
    f.next = &g;
    g.next = &h;
    h.next = &i;
    i.next = &j;
    j.next = &k;
    //Node* head = deleteDuplicateNode(&a);
    Node* head = deleteAllDuplicateNode(&a);
    print(head);
    return 0;
}
