//
// Created by Renyi on 2019-08-26.
//
#include <iostream>
#include <vector>
#include <queue>
using namespace std;

struct TreeNode{
    int value;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int v) : value(v), left(NULL), right(NULL) {}
};

vector<double> treeAverage(TreeNode* root){
    vector<double> avg;
    int sum = 0;
    int count = 1;
    queue<pair<TreeNode*, int>> q;

    if (root){
        q.push(make_pair(root, 0));
    }

    while (!q.empty()){
        TreeNode* node = q.front().first;
        int depth = q.front().second;
        q.pop();

        if(avg.size() == depth){
            avg.push_back(double(sum) / count);
            count = 1;
            sum = node->value;
        } else{
            sum += node->value;
            count++;
        }

        if (node->left){
            q.push(make_pair(node->left, depth+1));
        }
        if (node->right){
            q.push(make_pair(node->right, depth+1));
        }
    }
    avg.push_back(double(sum) / count);
    vector<double>::iterator k = avg.begin();
    avg.erase(k);
    return avg;
}

int main(){
    TreeNode a(1);//建立配图的二叉树
    TreeNode b(2);
    TreeNode c(5);
    TreeNode d(3);
    TreeNode e(4);
    TreeNode f(6);

    a.left = &b;
    a.right = &c;
    b.left = &d;
    b.right = &e;
    c.right = &f;

    vector<double> result = treeAverage(&a);
    for (int i = 0; i < result.size(); i++) {
        cout << result[i] << endl;
    }
    cout << endl;
    return 0;
}
