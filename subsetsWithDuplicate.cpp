//
// Created by renyi on 2019/6/26.
//
#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

void subsetsWithDuplicate(int i, vector<int> &nums, vector<int> &item, vector<vector<int>> &result, set<vector<int>> &res_set){
    if (i >= nums.size()){
        return;
    }
    item.push_back(nums[i]);
    if (res_set.find(item) == res_set.end()){//集合res_set中没有item，即去重逻辑，当第二次取到【1,2,2】时，将不会走if逻辑，result也不会追加item了，实现去重
        result.push_back(item);
        res_set.insert(item);
    }
    subsetsWithDuplicate(i + 1, nums, item, result, res_set);
    item.pop_back();
    subsetsWithDuplicate(i + 1, nums, item, result, res_set);
}

vector<vector<int>> subsets(vector<int> &nums){
    vector<int> item;
    vector<vector<int>> result;
    set<vector<int>> res_set;//去重的集合，需要将数组元素item插入集合中
    sort(nums.begin(), nums.end());//先将数组排序
    result.push_back(item);//结果中加入空集
    subsetsWithDuplicate(0, nums, item, result, res_set);
    return result;
}

int main(){
    vector<int> nums;
    nums.push_back(2);
    nums.push_back(1);
    nums.push_back(2);
    nums.push_back(2);
    vector<vector<int>> result;
    result = subsets(nums);
    for (int i = 0; i < result.size(); i++) {
        if (result[i].size() == 0){
            printf("[]");
        }
        for (int j = 0; j < result[i].size(); j++) {
            printf("[%d]", result[i][j]);
        }
        printf("\n");
    }
    return 0;
}