//
// Created by renyi on 2019-07-26.
//
#include <iostream>
#include <vector>
using namespace std;

void printArray(vector<int> nums){
    for (int i = 0; i < nums.size(); i++) {
        printf("[%d]", nums[i]);
    }
    printf("\n");
}

void swap(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}

void quickSort(vector<int> &nums, int low, int high){
    if (low >= high){
        return;
    }

    int i = low;
    int j = high;
    int base = nums[i];

    while (i < j){
        while (i < j && nums[j] >= base){
            j--;
        }
        swap(nums[i], nums[j]);

        while (i < j && nums[i] <= base){
            i++;
        }
        swap(nums[i], nums[j]);
    }

    quickSort(nums, low, i-1);
    quickSort(nums, i+1, high);
}

int main(){
    int temp[] = {7, 9, 5, 1, 2, 8, 6, 4, 0, 3, 10};
    vector<int> result;
    for (int i = 0; i < 11; i++) {
        result.push_back(temp[i]);
    }
    
    printArray(result);
    quickSort(result, 0, result.size()-1);
    printArray(result);
    
    return 0;
}

